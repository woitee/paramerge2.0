﻿using System;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ParaMerge2;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        private void GenerateRandomInputFile(string filename, int length) {
            Random r = new Random();
            StreamWriter sw = new StreamWriter(filename);
            for (int i = 0; i < length; ++i) {
                sw.WriteLine(r.Next());
            }
            sw.Close();
        }

        private void ParaSort(string input, string output) {
            TextWriter writer = new StreamWriter(input);
            TextReader reader = new StreamReader(output);
            try {
                ParaSorter<int> sorter = new ParaSorter<int>(reader, writer, (string s) => { return int.Parse(s); });

                sorter.Sort();
            } finally {
                writer.Close();
                reader.Close();
            }
        }

        private void SeqSort(string input, string output) {
            TextWriter writer = new StreamWriter(output);
            TextReader reader = new StreamReader(input);
            try {
                SeqSorter<int> sorter = new SeqSorter<int>(reader, writer, (string s) => { return int.Parse(s); });

                sorter.Sort();
            } finally {
                writer.Close();
                reader.Close();
            }
        }

        private void Test(int size, bool seq = false) {
            GenerateRandomInputFile("test.txt", size);

            if (seq) {
                SeqSort("sortedtest.txt", "test.txt");
            } else {
                ParaSort("sortedtest.txt", "test.txt");
            }

            StreamReader sr = new StreamReader("sortedtest.txt");
            int check = int.MinValue;
            string line; int counter = 0;
            //check ascending order and number of elements
            while ((line = sr.ReadLine()) != null) {
                ++counter;
                int a = int.Parse(line);
                Assert.IsTrue(a >= check);
                check = a;
            }
            sr.Close();
            Assert.AreEqual(size, counter);
        }

        [TestMethod]
        public void BenchmarkUndisclosed() {
            Benchmark(10*1000*1000);
        }

        private void Benchmark(int size) {
            string inputFile = "test.txt";
            string outputFile = "sortedtest.txt";
            GenerateRandomInputFile(inputFile, size);
            
            TextWriter statistics = new StreamWriter("statistics.txt");

            Stopwatch watch = new Stopwatch();

            watch.Start();
            SeqSort(inputFile, outputFile);
            watch.Stop();
            statistics.WriteLine("Sequential sort of {0} elements, clocked at: {1}", size, watch.Elapsed);

            watch.Reset();
            watch.Start();
            ParaSort(inputFile, outputFile);
            watch.Stop();
            statistics.WriteLine("Parallel sort of {0} elements, clocked at: {1}", size, watch.Elapsed);

            statistics.Close();
            StreamReader sr = new StreamReader("sortedtest.txt");
            int check = int.MinValue;
            string line; int counter = 0;
            //check ascending order and number of elements
            while ((line = sr.ReadLine()) != null) {
                ++counter;
                int a = int.Parse(line);
                Assert.IsTrue(a >= check);
                check = a;
            }
            sr.Close();
            Assert.AreEqual(size, counter);
        }

        [TestMethod]
        public void SortTinyFile() {
            Test(4);
        }

        [TestMethod]
        public void SortSmallFile() {
            Test(320);
        }

        [TestMethod]
        public void SortAllFileSizesLessThan100() {
            for (int i = 1; i <= 100; ++i) {
                Test(i);
            }
        }

        [TestMethod]
        public void SortBigFile() {
            Test(10 * 1000 * 1000);
        }

        [TestMethod]
        public void SortSeqBigFile() {
            Test(10 * 1000 * 1000, true);
        }
    }
}
