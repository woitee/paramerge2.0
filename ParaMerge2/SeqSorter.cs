﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("UnitTestProject1")]
namespace ParaMerge2 {
    class SeqSorter<T> {
        TextReader input;
        TextWriter output;
        Func<string, T> parseFromString;
        public SeqSorter(TextReader input, TextWriter output, Func<string, T> parseFromString) {
            this.input = input;
            this.output = output;
            this.parseFromString = parseFromString;
        }

        public void Sort() {
            var list = new List<T>();
            string line;
            while ((line = input.ReadLine()) != null) {
                list.Add(parseFromString(line));
            }
            list.Sort();
            foreach (T elem in list) {
                output.WriteLine(elem);
            }
        }
    }
}
