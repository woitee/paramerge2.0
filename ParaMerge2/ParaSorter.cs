﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ParaMerge2 {
    public class ParaSorter<T> where T : IComparable<T> {
        TextReader input;
        TextWriter output;
        Func<string, T> parseFromString;
        public ParaSorter(TextReader input, TextWriter output, Func<string, T> parseFromString) {
            this.input = input;
            this.output = output;
            this.parseFromString = parseFromString;
        }

        const int BLOCK_SIZE = 8192;

        private List<string> readBlock() {
            var list = new List<string>();
            string line;
            int count = 0;
            while ((line = input.ReadLine()) != null) {
                list.Add(line);
                if (++count >= BLOCK_SIZE) { break; }
            }
            return list;
        }

        private T[] parseAndSortList(List<string> list) {
            var array = new T[list.Count];
            int ix = 0;
            foreach (string s in list) {
                array[ix++] = parseFromString(s);
            }
            Array.Sort(array);
            return array;
        }

        private class TaskManager {
            Stack<Task<T[]>[]> incompleteTasks = new Stack<Task<T[]>[]>();

            public void AddData(Task<T[]> task) {
                Task<T[]>[] futureArray = new Task<T[]>[] { task };
                while (incompleteTasks.Count > 0 &&
                       incompleteTasks.Peek().Length == futureArray.Length) {
                    futureArray = MergeAsync(futureArray, incompleteTasks.Pop());
                }

                incompleteTasks.Push(futureArray);
            }

            public Task FinishAndWrite(TextWriter output) {
                Task<T[]>[] futureArray = incompleteTasks.Pop();
                while (incompleteTasks.Count > 0) {
                    futureArray = MergeAsync(futureArray, incompleteTasks.Pop());
                }

                return futureWriteToOutput(futureArray, output);
            }

            private Task<T[]>[] MergeAsync(Task<T[]>[] a, Task<T[]>[] b) {
                var result = new Task<T[]>[a.Length + b.Length];

                //exploiting shared nature of outer variables in lambda closures
                //the tasks created in this function are scheduled to run in order
                //and they use these variables listed here to track progress between them

                //we stick to the rule that small pointers are always valid if the big pointers are
                int aBigPtr = 0; int bBigPtr = 0;
                int aPtr = 0; int bPtr = 0;

                for (int i = 0; i < result.Length; ++i) {
                    //calculate prerequisities
                    var prerequisities = new List<Task<T[]>>();

                    if (i > 0) { prerequisities.Add(result[i-1]); }
                    if (i < a.Length) { prerequisities.Add(a[i]); }
                    if (i < b.Length) { prerequisities.Add(b[i]); }
                    
                    //perform future merge
                    result[i] = Task.Factory.ContinueWhenAll(
                        prerequisities.ToArray(),
                        (tasks) => {
                            return Merge.Block(a, ref aPtr, ref aBigPtr, b, ref bPtr, ref bBigPtr);
                        }
                    );

                    /*if (i > 0) {
                        result[i] = result[i - 1].ContinueWith(
                            (unused) => { return Merge.Block(a, ref aPtr, ref aBigPtr, b, ref bPtr, ref bBigPtr); }
                        );
                    } else {
                        result[i] = Task.Run(
                            () => {return Merge.Block(a, ref aPtr, ref aBigPtr, b, ref bPtr, ref bBigPtr);}
                        );
                    }*/

                }

                return result;
            }

            //NOTE: "small" pointers are always kept valid if big ones are
            //This functions tries to merge BLOCK_SIZE lowest elements
            //from an ascending array of ascending sequences (sequence futures)
            private static class Merge {
                public static T[] Block( Task<T[]>[] a, ref int aPtr, ref int aBigPtr, 
                                         Task<T[]>[] b, ref int bPtr, ref int bBigPtr) {
                    T[] result = new T[BLOCK_SIZE];
                    int ptr = 0;

                    if (aBigPtr >= a.Length) {
                        Transfer(b, ref bPtr, ref bBigPtr, result, ref ptr);
                        if (ptr < result.Length)
                            Trim(ref result, ptr);
                        return result;
                    }
                    if (bBigPtr >= b.Length) {
                        Transfer(a, ref aPtr, ref aBigPtr, result, ref ptr);
                        if (ptr < result.Length)
                            Trim(ref result, ptr);
                        return result;
                    }

                    T[] aArray = a[aBigPtr].Result;
                    T[] bArray = b[bBigPtr].Result;

                    while (ptr < result.Length) {
                        if (aArray[aPtr].CompareTo(bArray[bPtr]) < 0) {
                            //merge from first array
                            result[ptr++] = aArray[aPtr++];

                            if (aPtr >= aArray.Length) {
                                aPtr = 0; ++aBigPtr;
                                if (aBigPtr >= a.Length)
                                    break;
                                aArray = a[aBigPtr].Result;
                            }
                        } else {
                            //merge from second array
                            result[ptr++] = bArray[bPtr++];

                            if (bPtr >= bArray.Length) {
                                bPtr = 0; ++bBigPtr;
                                if (bBigPtr >= b.Length)
                                    break;
                                bArray = b[bBigPtr].Result;
                            }
                        }
                    }

                    if (ptr >= result.Length) { return result; }
                    Transfer(a, ref aPtr, ref aBigPtr, result, ref ptr);
                    if (ptr >= result.Length) { return result; }
                    Transfer(b, ref bPtr, ref bBigPtr, result, ref ptr);

                    if (ptr < result.Length) { Trim(ref result, ptr); }
                    return result;
                }

                private static void Transfer<S>(Task<S[]>[] source, ref int srcPtr, ref int srcBigPtr,
                                             S[] target, ref int targetPtr) {
                    
                    if (srcBigPtr >= source.Length) { return; }
                    S[] srcArray = source[srcBigPtr].Result;

                    do {
                        target[targetPtr++] = srcArray[srcPtr++];
                        if (targetPtr >= target.Length) {
                            //we have reached the end in target
                            //make sure srcPtr is valid if srcBigPtr is; before returning
                            if (srcPtr >= srcArray.Length) {
                                srcPtr = 0; ++srcBigPtr;
                            }
                            return;
                        }
                        //we have not yet reached the end, so make sure we have a valid location
                        //to copy from
                        if (srcPtr >= srcArray.Length) {
                            srcPtr = 0; ++srcBigPtr;
                            if (srcBigPtr >= source.Length) {
                                //we have reached the end in source
                                return;
                            }
                            srcArray = source[srcBigPtr].Result;
                        }
                    } while (true);
                }
                private static void Trim<S>(ref S[] array, int length) {
                    S[] result = new S[length];
                    Array.Copy(array, result, length);
                    array = result;
                }
            }

            private Task futureWriteToOutput(Task<T[]>[] futureData, TextWriter output) {
                //create a task that writes the first part of the result
                Task<T[]> lastWriteTask = futureData[0].ContinueWith(
                    (completedTask) => { foreach (T elem in completedTask.Result) {
                                            output.WriteLine(elem);
                                        }
                                        //not used for anything, needed to fit the Task<T[]> type
                                        return (T[])null; 
                                    }
                );

                //write all other parts of the result, always waiting for the previous one
                for (int i = 1; i < futureData.Length; ++i) {
                    lastWriteTask = Task.Factory.ContinueWhenAll
                        (new[] { lastWriteTask, futureData[i] },

                        (tasks) => {
                            foreach (T elem in tasks[1].Result) {
                                output.WriteLine(elem);
                            }
                            //never used, needed to fit the Task<T[]> type
                            return (T[])null;
                            }
                        );
                }

                //create a task the caller can wait for finishing
                return lastWriteTask.ContinueWith((unused) => { });
            }
        }
        TaskManager taskManager = new TaskManager();


        public void Sort() {
            TaskFactory factory = Task.Factory;

            do {
                //read a chunk of data
                List<string> list = readBlock();
                //do not process empty lists, it complicates inner code
                if (list.Count == 0) { break; }

                //process the data
                var sortTask = Task<T[]>.Run(
                    () => {
                        return parseAndSortList(list);
                    });
                taskManager.AddData(sortTask);

                //if not successful in reading full length requested data, stop reading
                if (list.Count != BLOCK_SIZE) break;
            } while (true);

            //after the base of the future tasks has been set
            //create tasks that will finish merging, and write result to output
            Task lastWriteTask = taskManager.FinishAndWrite(output);

            //wait for the whole program to finish executing
            lastWriteTask.Wait();
        }
    }
}
