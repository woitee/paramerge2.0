﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ParaMerge2 {
    public class ProgramBody {
        public static void Execute(string[] args) {
            try {
                if (args.Length != 2) {
                    throw new ArgumentException("Invalid number of arguments");
                }
                string inputFile = args[0], outputFile = args[1];
                TextWriter writer = new StreamWriter(outputFile);

                TextReader reader = new StreamReader(inputFile);
                try {
                    ParaSorter<int> sorter = new ParaSorter<int>(reader, writer, (string s) => { return int.Parse(s); });

                    sorter.Sort();
                } finally {
                    writer.Close();
                    reader.Close();
                }
            } catch (ArgumentException) {
                Console.WriteLine("Argument Error");
            } catch (FormatException) {
                Console.WriteLine("Format Error");
            } catch (OverflowException) {
                Console.WriteLine("Format Error");
            } catch (IOException) {
                Console.WriteLine("IO Error");
            } catch (UnauthorizedAccessException) {
                Console.WriteLine("IO Error");
            } catch (System.Security.SecurityException) {
                Console.WriteLine("IO Error");
            }
        }
    }

    class Program {
        static void Main(string[] args) {
            //expects command line arguments [inputfile] [outputfile]
            ProgramBody.Execute(args);
        }
    }
}
